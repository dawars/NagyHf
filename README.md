![Build Status](https://gitlab.com/dawars00/NagyHf/badges/master/build.svg)

Git repo: https://gitlab.com/dawars00/NagyHf

Documentation: http://dawars00.gitlab.io/NagyHf/

Table of Content:

* Classes:
    * Graph
    * DirectedGraph
    * BFS
    * DFS
    * NonrecursiveDFS
    * CC

* Implemented Algorithmen
    * Depth First Search
    * Non-recursive Depth First Search
    * Breath First Search
    * Connected Components
* Future work:
    * Flow (Min cut - max flow)
    * Dijkstra
    * Topological Sort

[Gleiche in Deutsch]